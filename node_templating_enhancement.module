<?php
/**
 * @file
 * file for module node_templating_enhancement.
 * @author Eric Browet <broweteric@gmail.com>
 */

/**
 * @author Eric Browet <broweteric@gmail.com>
 * Implement hook_menu().
 */
function node_templating_enhancement_menu() {
  $items = array();
  $items['admin/config/system/node_templating'] = array(
    'title'            => 'Node templating enhancement',
    'description'      => 'Control the node templating possibility',
    'page callback'    => 'drupal_get_form',
    'page arguments'   => array('node_templating_enhancement_manage'),
    'access arguments' => array('Administer node templating'),
    'type'             => MENU_NORMAL_ITEM
  );
  return $items;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * Implement hook_help().
 */
function node_templating_enhancement_help($path, $arg) {
  switch ($path) {
    case 'admin/help#node_templating_enhancement':
      /**
       * create a example
       */
      /**
       * content type array
       */
      $types      = _get_content_types();
      /**
       * languages array
       */
      $lg         = _get_languages();
      /**
       * we get the first element for default value
       */
      $type       = key($types);
      /**
       * we get the first element for default value
       */
      $l          = key($lg);
      /**
       * we set values to have default values
       */
      $type       = variable_get('nte_choose_content_type', $type);
      $l          = variable_get('nte_choose_languages', $l);
      $nid        = variable_get('nte_choose_nid', 1);
      $identifier = variable_get('nte_choose_identifier', 'legal_mentions');
      $view_mode  = variable_get('nte_choose_view_mode', 'teaser');
      //enabled/disabled array
      $status_array       = array( 1 => '<span style="color:green;font-weight:bold">(' . t('Enabled') . ')</span>', 0 => '<span style="color:red;font-weight:bold">' . t('Disabled') . '</span>');
      //conf
      $language_enabled   = variable_get('language_mapping_enabled', 0);
      $view_mode_enabled  = variable_get('view_mode_mapping_enabled', 0);
      $identifier_enabled = variable_get('identifier_mapping_enabled', 0);
      $promote_enabled    = variable_get('promote_mapping_enabled', 0);
      //display
      $output = '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('This module add theme hook suggestions to a node . it\'s could be by matching the site language, the view mode of the node, the "promoted to front page" attribute of the node, or the "identifier" of the node (identifier is a new feature implement by this module). If you want to add your theme hook suggestions, you could use "hook" of this module ("hook_nte_add_suggestions_before", "hook_nte_add_suggestions_after", "hook_nte_suggestions_alter"), see the node templating API.') . '</p>';
      $output .= '<h3>' . t('Uses') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt style="font-weight:bold">' . t('Theme hook suggestions by view_mode') . ' ' . $status_array[$view_mode_enabled] . '</dt>';
      $output .= '<dd>' . _get_view_mode_explain() . '</dd>';
      $output .= '<dt style="font-weight:bold">' . t('Theme hook suggestions by language') . ' ' . $status_array[$language_enabled] . '</dt>';
      $output .= '<dd>' . _get_language_mapping_explain() . '</dd>';
      $output .= '<dt style="font-weight:bold">' . t('Theme hook suggestions by identifier') . ' ' . $status_array[$identifier_enabled] . '</dt>';
      $output .= '<dd>' . _get_identifier_mapping_explain() . '</dd>';
      $output .= '<dt style="font-weight:bold">' . t('Themes hook suggestions by "promoted to front page"') . ' ' . $status_array[$promote_enabled] . '</dt>';
      $output .= '<dd>' . _get_promote_mapping_explain() . '</dd>';
      $output .= '</dl>';
      $output .= '<h3>' . t('Examples (with current settings)') . '</h3>';
      $output .= '<h4>' . t('Generic example') . '</h4>';
      $output .= drupal_render(_get_current_templating_possibility());
      $output .= '<h4>' . t('Context example (set in configuration page)') . '</h4>';
      $output .= t('Example for a node with nid : %nid and content type : %type', array('%nid' => $nid, '%type' => $type));
      if ($identifier_enabled) {
        $output .= ' ' . t('and identifier : %identifier', array('%identifier' => $identifier));
      }
      if ($view_mode_enabled) {
        $output .= ' ' . t('and view_mode : %view_mode', array('%view_mode' => $view_mode));
      }
      if ($promote_enabled) {
        $output .= ' ' . t('and promoted to front page');
      }
      if ($language_enabled) {
        $output .= ' ' . t('and language : %lg', array('%lg' => $l));
      }
      $output .= drupal_render(_get_example_templating_possibility($type, $view_mode, $l, $identifier, $nid));
      return $output;
  }
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * Implement hook_form_alter().
 */
function node_templating_enhancement_form_alter(&$form, $form_state, $form_id) {
  $nid = '';
  if (isset($form['#node_edit_form']) && $form['#node_edit_form'] && variable_get('identifier_mapping_enabled', FALSE) && user_access('set a template identifier')) {
    if (isset($form['nid']['#value']) && $form['nid']['#value'] != NULL) {
      $nid = $form['nid']['#value'];
    }
    //use machine man form element
    $form['nte_identifier'] = array(
      '#type' => 'machine_name',
      '#title'          => t('Templating identifier'),
      '#default_value'  => variable_get('nte_indentifier_' . $nid, ''),
      // This field should stay LTR even for RTL languages.
      '#field_prefix' => '<span dir="ltr">',
      '#field_suffix' => '</span>&lrm;',
      '#size' => 15,
      '#description' => t('A unique machine-readable name containing letters, numbers, and underscores.') . ' ' . t('Use with caution, usefull for particular case not for all node.'),
      // 32 characters minus the 'field_' prefix.
      '#maxlength' => 15,
      '#prefix' => '<div class="add-new-placeholder">&nbsp;</div>',
      '#machine_name' => array(
        'source' => array(),
        'exists' => '_nte_identifier_exist',
        'standalone' => TRUE,
        'label' => '',
  ),
      '#required' => FALSE,
      );
  }
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * callback function
 * use to check if the nte_identifier exist
 * @param
 * $value string to check
 * @return
 * Boolean
 */
function _nte_identifier_exist($value) {
  //allow same
  return FALSE;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * Implement hook_node_submit().
 */
function node_templating_enhancement_node_submit($node, $form, &$form_state) {
  if (variable_get('identifier_mapping_enabled', FALSE)) {
    if (!empty($form_state['values']['nte_identifier'])) {
      variable_set('nte_indentifier_' . $node->nid, $form_state['values']['nte_identifier']);
    }
  }
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * Implement hook_node_load().
 */
function node_templating_enhancement_node_load($nodes, $types) {
  if (variable_get('identifier_mapping_enabled', FALSE)) {
    /**
     * better and faster than a foreach in our case;
     */
    $nodes = array_map('_add_identifier', $nodes);
  }
}

/**
 * @author Eric Browet <broweteric@gmail.com>
 * Implement hook_permission().
 * return array of permission
 */
function node_templating_enhancement_permission() {
  return array(
    'Administer node templating' => array(
      'title'       => t('Administer the node templating'),
      'description' => t('Control the node templating possibility.')
  ),
    'set a template identifier' => array(
      'title'       => t('Set a template identifier'),
      'description' => t('Allow user to set a identifier to a node.'),
  ));
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * Implement hook_preprocess_node().
 */
function node_templating_enhancement_preprocess_node(&$variables) {
  if (isset($variables['view_mode'])) {
    $variables['theme_hook_suggestions'] = _add_templating($variables['theme_hook_suggestions'], $variables);
  }
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * return true if we display the page of the node
 * @return Boolean
 */
function _is_node_page_context($nid) {
  $a0 = arg(0);
  $a1 = arg(1);
  $a2 = arg(2);
  if ($a0 == 'node' && $a1 == $nid && $a2 == NULL) {
    return TRUE;
  }
  return FALSE;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * add templating possibility
 * @param
 * $suggestion array of theme hook suggestions pass by reference
 * @param
 * $variables array $variables of hook_preprocess_node()
 * @return void
 */
function _add_templating(array &$suggestions, $variables) {
    global $language;
    /**
     * get the page context to activate or not the promote suggestion
     */
    $node_page_context  =_is_node_page_context($variables['nid']);
    /**
     * verify identifier
     */
    if (!empty($variables['nte_identifier'])) {
      $identifier = $variables['nte_identifier'];
    } 
    else {
      $identifier = variable_get('nte_indentifier_' . $variables['nid'], FALSE);
    }
    /**
     * identifier suggestion only if availabled
     */
    if ($identifier && variable_get('identifier_mapping_enabled', FALSE)) {
      $suggestions[] = 'node__' . $identifier;
    }
    /**
     * search if some module add suggestion to pass trought the suggestion enhancement
     */
    $suggestions_before = module_invoke_all('nte_add_suggestions_before', $suggestions, $variables);
    $suggestions        = array_merge($suggestions, $suggestions_before);
    //just for 'FOR'
    $count              = count($suggestions);
    /**
     * we need an array to add theme hook suggestion without lost the overriding 
     * template order (more common to more specific)
     */
    $array              = array();
    // 'for' is my ennemies @todo search better method to insert suggestion
    // we keep order to add suggestion
    
    //to keep safe the override suggestions order, we separe the languge case
    //so we just have to check once and not for every other case possible
    if (variable_get('language_mapping_enabled', FALSE)) {
      for ($i = 0; $i < $count; $i++) {
        $array[] = $suggestions[$i];
        if (variable_get('view_mode_mapping_enabled', FALSE)) {
          $array[] = $suggestions[$i] . '__language_' . $language->language;
          $array[] = $suggestions[$i] . '__' . $variables['view_mode'];
          $array[] = $suggestions[$i] . '__' . $variables['view_mode'] . '__language_' . $language->language;
        }
        //@TODO add identifier possibility

        // dont had promote suggestion if the current page page display the node
        // promote are genrally use for front page context
        if ($variables['promote'] && variable_get('promote_mapping_enabled', FALSE) && !$node_page_context) {
          $array[] = $suggestions[$i] . '__promote_to_front';
          $array[] = $suggestions[$i] . '__promote_to_front' . '__language_' . $language->language;
        }
        //@TODO add Random possibility
      }
      $suggestions = $array;
      //add this entry in first possibility to keep logis of overriding template
      //the more common to the more specific

      // dont had promote suggestion if the current page page display the node
      // promote are genrally use for front page page context
      if ($variables['promote'] && variable_get('promote_mapping_enabled', FALSE) && !$node_page_context) {
        array_unshift($suggestions, 'node__promote_to_front' . '__language_' . $language->language);
        array_unshift($suggestions, 'node__promote_to_front');
      }
      if (variable_get('view_mode_mapping_enabled', FALSE)) {
        array_unshift($suggestions, 'node__' . $variables['view_mode'] . '__language_' . $language->language);
        // to be the first must be declare the last
        array_unshift($suggestions, 'node__' . $variables['view_mode']);
      }
      array_unshift($suggestions, 'node__language_' . $language->language);
    }
    else {
      for ($i = 0; $i < $count; $i++) {
        $array[] = $suggestions[$i];
        if (variable_get('view_mode_mapping_enabled', FALSE)) {
          $array[] = $suggestions[$i] . '__' . $variables['view_mode'];
        }
        //@TODO add identifier possibility

        // dont had promote suggestion if the current page page display the node
        // promote are genrally use for front page context
        if ($variables['promote'] && variable_get('promote_mapping_enabled', FALSE) && !$node_page_context) {
          $array[] = $suggestions[$i] . '__promote_to_front';
        }
        //@TODO add Random possibility
      }
      $suggestions = $array;
      //add this entry in first possibility to keep logis of overriding template
      //the more common to the more specific

      // don't had promoted suggestion if the current page page display the node
      // promoted are genrally use for promoted to front page context.
      if ($variables['promote'] && variable_get('promote_mapping_enabled', FALSE) && !$node_page_context) {
        array_unshift($suggestions, 'node__promote_to_front');
      }
      if ($variables['promote'] && variable_get('view_mode_mapping_enabled', FALSE)) {
        // to be the first must be declare the last
        array_unshift($suggestions, 'node__' . $variables['view_mode']);
      }
    }
    /**
     * search if some just want add own suggestions
     */
    $suggestions_hook = module_invoke_all('nte_add_suggestions_after', $suggestions, $variables);
    $suggestions = array_merge($suggestions, $suggestions_hook);
    /**
     * give the possibility to re order or rebuild the suggestions
     */
    drupal_alter('nte_suggestions', $suggestions);
}
/**
 * @author Eric Browet <broweteric@gmail.com>s
 * to get example of node templating enhancement theme hook suggestions by using
 * the configuration's settings. 
 * 
 * @return array of theme hook sugestions example
 */
function _get_current_templating_possibility() {
  $identifier   = 'ma_page_de_garde';
  $suffix       = '.tpl.php';
  $suggestions  = array( 'node--[content type]', 'node--[node nid]');
  if ($identifier) {
    $suggestions[] = 'node--[identifier]';
  }
  $suggestions_before = module_invoke_all('nte_add_suggestions_example_before', $suggestions);
  $suggestions        = array_merge($suggestions, $suggestions_before);

  $count              = count($suggestions);
  $array              = array();
  if (variable_get('language_mapping_enabled', FALSE)) {
    for ($i = 0; $i < $count; $i++) {
      $array[] = $suggestions[$i] . $suffix;
      if (variable_get('view_mode_mapping_enabled', FALSE)) {
        $array[] = $suggestions[$i] . '--language-[language->language]' . $suffix;
        $array[] = $suggestions[$i] . '--[view_mode]' . $suffix;
        $array[] = $suggestions[$i] . '--[view_mode]--language-[language->language]' . $suffix;
      }
      if (variable_get('promote_mapping_enabled', FALSE)) {
        $array[] = $suggestions[$i] . '--promote-to-front' . $suffix;
        $array[] = $suggestions[$i] . '--promote-to-front--language-[language->language]' . $suffix;
      }
    }
    $suggestions = $array;
    if (variable_get('promote_mapping_enabled', FALSE)) {
      array_unshift($suggestions, 'node--promote-to-front--language-[language->language]' . $suffix);
      array_unshift($suggestions, 'node--promote-to-front' . $suffix);
    }
    if (variable_get('view_mode_mapping_enabled', FALSE)) {
      array_unshift($suggestions, 'node--[view_mode]--language-[language->language]' . $suffix);
      array_unshift($suggestions, 'node--[view_mode]' . $suffix);
    }
    array_unshift($suggestions, 'node--language-[language->language]' . $suffix);
  }
  else {
    for ($i = 0; $i < $count; $i++) {
      $array[] = $suggestions[$i] . $suffix;
      if (variable_get('view_mode_mapping_enabled', FALSE)) {
        $array[] = $suggestions[$i] . '--[view_mode]' . $suffix;
      }
      if (variable_get('promote_mapping_enabled', FALSE)) {
        $array[] = $suggestions[$i] . '--promote-to-front' . $suffix;
      }
    }
    $suggestions = $array;
    if (variable_get('promote_mapping_enabled', FALSE)) {
      array_unshift($suggestions, 'node--promote-to-front' . $suffix);
    }
    if (variable_get('view_mode_mapping_enabled', FALSE)) {
      array_unshift($suggestions, 'node--[view_mode]' . $suffix);
    }
  }
  $suggestions_hook = module_invoke_all('nte_add_suggestions_example_after', $suggestions);
  $suggestions = array_merge($suggestions, $suggestions_hook);

  drupal_alter('nte_add_suggestions_example', $suggestions);

  return array('#theme' => 'item_list', '#items' => $suggestions);
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * to get example of node templating enhancement theme hook suggestions by using
 * the configuration's settings and simulated data.
 * @return array of theme hook sugestions example
 */
function _get_example_templating_possibility($content_type = 'article', $view_mode ="teaser", $language = 'fr', $identifier = 'legal-mention', $nid = 1) {
  $possibility = _get_current_templating_possibility();
  $replace['search'] = array('[content type]', '[view_mode]', '[language->language]', '[identifier]', '[node nid]');
  $replace['replace'] = array(str_replace('_', '-', $content_type), str_replace('_', '-', $view_mode), str_replace('_', '-', $language), str_replace('_', '-', $identifier), $nid);
  array_walk($possibility['#items'], '_replace_with', $replace);
  return $possibility;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * callback function replace generic example with simulated data
 */
function _replace_with(&$suggestions, $key, $replace) {
  $suggestions = str_replace($replace['search'], $replace['replace'], $suggestions);
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * return explanation of template suggestion with languages
 * @return string HTML
 */
function _get_language_mapping_explain() {
  $output   = '<div>';
  $output  .= t('If enabled theme hook suggestions use language.<p><u>For example :</u><br/><b>node--language--[language->language].tpl.php</b><br/>If your site is in <i>french</i>, you can use <i>node--language-fr.tpl.php</i> to render a node. Mapping language is add for every suggestion possibilty, like this you always keep the possibility to have a different display for different language.</p>'); 
  $output  .= '</div>';
  return $output;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * return explanation of template suggestion with promoted to front attribute
 * @return string HTML
 */
function _get_promote_mapping_explain() {
  $output   = '<div>';
  $output  .= t('If enabled theme hook suggestions use "promoted to front page" of the node.<p><u>For example :</u><br/><b>node--promote-to-front.tpl.php</b><br/>Because promoted to front page is usualy for front page this theme hook suggestions is not add in "htpp://your_domain.domain/node/nid" context.</p>'); 
  $output  .= '</div>';
  return $output;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * return explanation of template suggestion with view_mode
 * @return string HTML
 */
function _get_view_mode_explain() {
  $output  = '<div>';
  $output .= t('If enabled theme hook suggestions use view_mode. It\'s could be natural view mode like teaser, or a view mode set in a node_view().<p><u>For example :</u><br/><b>node--[view_mode].tpl.php</b><br/>If you use <i>drupal_render(node_view($my_node, \'listing\'))</i> you could render a node by using <i>node--listing.tpl.php</i>, if the node is content type <i>article</i> you could render by using <i>node--article--listing.tpl.php</i>.</p>');
  $output .= '</div>';
  return $output;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * return explanation of template suggestion by identifier
 * @return string HTML
 */
function _get_identifier_mapping_explain() {
  $output    = '<div>';
  $output  .= t('If enabled theme hook suggestions use node identifier.<p><u>For example :</u><br/><b>node--[identifier].tpl.php</b><br/>Node identifier is a new feature add by node templating enhancement. When you create a node you could add a identifier. Usually when you want to have a special display for a special node you use node--[nid].tpl.php, but generally in your production enviroment this node don\'t have the same nid than in your development environement. if you use identifier you can have the same identifier in production and developement. Use it with caution and don\'t abuse of this new feature.</p>'); 
  $output  .= '</div>';
  return $output;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * form to manage node templating
 * @return form array
 */
function node_templating_enhancement_manage($form, &$form_state) {
  $form = array();
  $form['level_templating'] = array(
    '#title'  => t('Level of templating'),
    '#type'   => 'fieldset',
    '#description'   => t('Choose which theme hook suggestion is enabled.')
    );
  $form['level_templating']['view_mode_mapping_enabled'] = array(
    '#title'         => t('Enabled view mode'),
    '#type'          => 'checkbox',
    '#default_value' => variable_get('view_mode_mapping_enabled', 0),
    '#description'   => _get_view_mode_explain(),
    );
  $form['level_templating']['language_mapping_enabled'] = array(
    '#title'         => t('Enabled language'),
    '#type'          => 'checkbox',
    '#default_value' => variable_get('language_mapping_enabled', 0),
    '#description'   => _get_language_mapping_explain(),
    );
  $form['level_templating']['promote_mapping_enabled'] = array(
    '#title'         => t('Enabled promote to front page mapping'),
    '#type'          => 'checkbox',
    '#default_value' => variable_get('promote_mapping_enabled', 0),
    '#description'   => _get_promote_mapping_explain(),
    );
  $form['level_templating']['identifier_mapping_enabled'] = array(
    '#title'         => t('Enabled node identifier'),
    '#type'          => 'checkbox',
    '#default_value' => variable_get('identifier_mapping_enabled', 0),
    '#description'   => _get_identifier_mapping_explain(),
    );
  $form['tosee'] = array(
    '#type' => 'vertical_tabs',
  );
  $form['explains'] = array(
    '#title'      => t('Generic example'),
    '#type'       => 'fieldset',
    '#group'      => 'tosee',
    '#description' => t('Here the theme hook suggestion\'s possibility.'),
    );
  $form['explains']['explain'] = array(
    '#markup' => drupal_render(_get_current_templating_possibility()),
  );
  $form['examples'] = array(
    '#title'       => t('Context example'),
    '#type'        => 'fieldset',
    '#group'       => 'tosee',
    '#description' => t('Here you can enter some values to simulate the theme hook suggestion\'s possibility.'),
    );

  /**
   * create example
   */
  /**
   * content type array
   */
  $types      = _get_content_types();
  /**
   * languages array
   */
  $lg         = _get_languages();
  /**
   * we get the first element to have default value
   */
  $type       = key($types);
  /**
   * we get the first element to have default value
   */
  $l          = key($lg);
  /**
   * we set values to have default values
   */
  $nid        = 1;
  $view_mode  = 'teaser';
  $identifier = 'legal_mentions';

  //always enabled
  $type = variable_get('nte_choose_content_type', $type);
  $form['examples']['nte_choose_content_type'] = array(
    '#type'           => 'select',
    '#options'        => $types,
    '#default_value'  => $type,
    '#title'          => t('Choose a content type'),
  );
  //only if enabled
  if (variable_get('language_mapping_enabled', FALSE)) {
    $l = variable_get('nte_choose_languages', $l);
    $form['examples']['nte_choose_languages'] = array(
      '#type'           => 'select',
      '#options'        => $lg,
      '#default_value'  => $l,
      '#title'          => t('Choose a language'),
    );
  }
  //always enabled
  $nid = variable_get('nte_choose_nid', $nid);
  $form['examples']['nte_choose_nid'] = array(
    '#type'           => 'textfield',
    '#default_value'  => $nid,
    '#title'          => t('Enter a node nid'),
    '#size'           => 10,
  );
  //only if enabled
  if (variable_get('identifier_mapping_enabled', FALSE)) {
    $identifier = variable_get('nte_choose_identifier', $identifier);
    $form['examples']['nte_choose_identifier'] = array(
      '#type'           => 'textfield',
      '#default_value'  => $identifier,
      '#title'          => t('Enter a identifier'),
      '#size'           => 10,
    );
  }
  //only if enabled
  if (variable_get('view_mode_mapping_enabled', FALSE)) {
    $view_mode = variable_get('nte_choose_view_mode', $view_mode);
    $form['examples']['nte_choose_view_mode'] = array(
      '#type'           => 'textfield',
      '#default_value'  => $view_mode,
      '#title'          => t('Enter a view_mode'),
      '#size'           => 10,
    );
  }
  $form['examples']['example'] = array(
    '#markup' => drupal_render(_get_example_templating_possibility($type, $view_mode, $l, $identifier, $nid)),
  );
  return system_settings_form($form);
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * return an array of content types
 */
function _get_content_types() {
  $types = node_type_get_types();
  $types = array_map('_format_content_type_array', $types);
  return $types;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * return an array of languages
 * @return array
 */
function _get_languages() {
  $lg = language_list();
  $lg = array_map('_format_languages_array', $lg);
  return $lg;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * callback function to format the array of content types
 */
function _format_content_type_array($type) {
  return $type->name;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * callback function to format the array of languages
 */
function _format_languages_array($lg) {
  return $lg->name;
}
/**
 * @author Eric Browet <broweteric@gmail.com>
 * callback function to add the identifier to the node.
 */
function _add_identifier($node) {
  $node->nte_identifier = variable_get('nte_identifier' . $node->nid, FALSE);
  return $node;
}
