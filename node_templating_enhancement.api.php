<?php
/**
 * @file
 * Hooks provided by the Node templating enhancement module.
 * @author Eric Browet <broweteric@gmail.com>
 */

/**
 * @defgroup node_templating_enhancement_api_hooks Node API Hooks
 * @{
 * Functions to add and modify the theme_hook_suggestions.
 *
 * Each node have a basic theme_hook_suggestions, Node templating enhancement
 * hook the hook_preprocess_node() to add new theme_hook_suggestions. Node 
 * templating enhancement use his back office settings to add  
 * theme_hook_suggestions. During the process some node templating
 * enhancement hook are use. 
 * Here the list
 *   - hook_nte_add_suggestions_before()
 *   - hook_nte_add_suggestions_after()
 *   - hook_nte_suggestions_alter()
  * @}
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Before node templating enhancement add theme hook suggestions. the theme
 * hook suggestions adding in this hook, will be use by node templating enhancement.
 * If 'node_mytheme_hook_suggestion is add, node templating enhancement add his
 * possibily(ie node_mytheme_hook_suggestion__language_fr, node_mytheme_hook_suggestion__[view_mode] etc).
 * @author Eric Browet <broweteric@gmail.com>
 * @code
 * return array('my_theme_sugestions_1', 'my_theme_sugestions_2');
 * 
 * @see hook_preprocess_node()
 *
 * @param $suggestions
 *   existing theme_hook_suggestions of the node.
 * @param $variables
 *   the $variables of the hook_preprocess_node().
 *
 * @return
 *   An array of personnal theme_hook_suggestions
 *
 * @ingroup nte_add_suggestions
 */
function hook_nte_add_suggestions_before($suggestions, $variables) {
  $identifier = FALSE;
  if (!empty($variables['nte_identifier'])) {
    $identifier = $variables['nte_identifier'];
  } 
  else {
    $identifier = variable_get('nte_indentifier_' . $variables['nid'], FALSE);
  }
  if ($identifier) {
    return array('node__edit__' . $identifier);
  }
}
/**
 * After node templating enhancement add theme hook suggestion. the theme
 * hook suggestions adding in this hook, will not be use by node templating 
 * enhancement.
 * @author Eric Browet <broweteric@gmail.com>
 * @code
 * return array('my_theme_sugestions_1','my_theme_sugestions_2');
 * 
 * @see hook_preprocess_node()
 *
 * @param $suggestions
 *   existing theme_hook_suggestions of the node.
 * @param $variables
 *   the $variables of the hook_preprocess_node().
 *
 * @return
 *   An array of personnal theme_hook_suggestions
 *
 * @ingroup nte_add_suggestions
 */
function hook_nte_add_suggestions_after($suggestion, $variables) {
  return array('node__' . $variables['vid']);
}
/**
 * After node templating enhancement add theme hook suggestions. you can sort the
 * theme hook suggestions array, or add/delete/modify theme hook suggestion.
 * @author Eric Browet <broweteric@gmail.com>
 * @see hook_preprocess_node()
 *
 * @param $suggestions
 *   existing theme_hook_suggestions of the node.
 * @param $variables
 *   the $variables of the hook_preprocess_node().
 *
 * @return
 * void
 *
 * @ingroup nte_add_suggestions
 */
function hook_nte_suggestions_alter(&$suggestions, $variables) {
  shuffle($suggestions);
}

/**
 * Before node templating enhancement add example theme hook suggestions.
 * @author Eric Browet <broweteric@gmail.com>
 * @code
 * return array('node--my-theme-sugestions-1', 'node--my-theme-sugestions-2');
 * 
 * @see hook_preprocess_node()
 *
 * @param $suggestions
 *   existing theme_hook_suggestions example.
 *
 * @return
 *   An array of theme_hook_suggestions example.
 *
 * @ingroup nte_add_suggestions_exapmle
 */
function sandbox_nte_add_suggestions_example_before($suggestions) {
  return array('node--edit--[identifier]');
}
/**
 * After node templating enhancement add theme hook suggestion example.
 * @author Eric Browet <broweteric@gmail.com>
 * @code
 * return array('node--my-theme-sugestions-1.tpl.php','node--my-theme-sugestions-2.tpl.php');
 * 
 * @see hook_preprocess_node()
 *
 * @param $suggestions
 *   existing theme_hook_suggestions example.
 *
 * @return
 *   An array of theme_hook_suggestions example
 *
 * @ingroup nte_add_suggestions_example
 */
function sandbox_nte_add_suggestions_example_after($suggestions) {
  return array('node--[vid].tpl.php');
}
/**
 * After node templating enhancement add theme hook suggestions example. you can sort the
 * theme hook suggestions example array, or add/delete/modify theme hook suggestion example.
 * @author Eric Browet <broweteric@gmail.com>
 * @see hook_preprocess_node()
 *
 * @param $suggestions
 *   existing theme_hook_suggestions example.
 *
 * @return
 * void
 *
 * @ingroup nte_add_suggestions_example
 */
function sandbox_nte_add_suggestions_example_alter(&$suggestions) {
  $suggestions[] = 'node--sandbox.tpl.php';
}