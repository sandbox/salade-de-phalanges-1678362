********************************************************************************
Node templating enhancement
********************************************************************************
1)Install the module
====================
Go to /admin/modules check Node templating enhancement click save configuration.

2)Permission
============
Go to /admin/people/permissions#module-node_templating_enhancement
check "Administer the node templating" for the rôles who can Control the node 
templating possibility. 
check "Set a template identifier" to give the possibilty to user to set a 
identifier on a node.

3)Configure
===========
Go to /admin/config/system/node_templating to configure the theme hook 
suggestion possibility.

4)Code
======
To modify or add theme suggestions you could use hook of node templating 
enhancement. See node_templating_enhancement.api.php to do this. You could use
nte_sanbox module to make your own test.

For more details, see:
 http://drupal.org/node/
 http://[your_site]/admin/help/node_templating_enhancement

To ask question wrote : broweteric@gmail.com subject : DRUPAL 7 NODE TEMPLATING 
ENHANCEMENT MODULE